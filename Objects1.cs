﻿namespace Finalizers {
    class A {
        public A() {
            System.Console.WriteLine("I've been created");
        }

         ~A() {
            System.Console.WriteLine("I've been removed");
        }

    }
    class B : A {
        public B() {
            System.Console.WriteLine("I've been created");
        }

        ~B() {
            System.Console.WriteLine("I've been removed");
        }

    }
}
