﻿using System;

namespace Finalizers {
    class Program {
        static void Main() {
            // create object
            B myObject = new B();

            // "remove" object
            //myObject = null;

            //// display how much memory is being used (so that we can compare later)
            Console.WriteLine("Memory used before garbage collection:\t" + GC.GetTotalMemory(false));

            GC.Collect();

            GC.WaitForPendingFinalizers();

            //// display how much memory is being used (so that we can see, that the object is actually being removed)
            Console.WriteLine("Memory used before garbage collection:\t" + GC.GetTotalMemory(true));

            Console.ReadKey();
        }
    }
}
